package java_day01;
import java.util.Date; // 匯入 Date 類別
import java.time.LocalDateTime; // 匯入 LocalDateTime 類別
import java.time.format.DateTimeFormatter; // 匯入 DateTimeFormatter 類別
import javax.swing.*; //https://www.youtube.com/watch?v=GBzwSf8bEdQ 

public class HelloWorld {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println("Hello, World");
		
        // 使用 java.util.Date
        Date currentDate = new Date();
        System.out.println("Current date and time: " + currentDate);

        // 使用 java.time.LocalDateTime
        LocalDateTime currentDateTime = LocalDateTime.now();
        System.out.println("Current date and time: " + currentDateTime);

        // 格式化輸出
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        String formattedDateTime = currentDateTime.format(formatter);
        System.out.println("Formatted date and time: " + formattedDateTime);		
        
        // 建立 JFrame 物件，表示應用程式的視窗
        JFrame frame = new JFrame("My Window");
        // 設置視窗關閉時的行為
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        // 建立一個 JLabel 物件，顯示文字內容
        JLabel label = new JLabel("Hello, World!");
        // 將 JLabel 加入到視窗中
        frame.getContentPane().add(label);
        // 設置視窗大小
        frame.setSize(300, 200);
        // 設置視窗可見
        frame.setVisible(true);
	}


}
